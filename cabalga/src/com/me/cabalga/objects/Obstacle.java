package com.me.cabalga.objects;

import com.me.cabalga.game.DynamicGameObject;
import com.me.cabalga.world.Constants;

/**
 * Clase objeto obstáculo
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class Obstacle extends DynamicGameObject{

	float stateTime;
	Floor myFloor;
	float myDeviation;
	
	/**
	 * Obstáculo que contiene textura y posición
	 * @param x posición de coordenadas del obstáculo
	 * @param y posición de ordenadas del obstáculo
	 * @param type constante tipo de obstáculo para su textura
	 */
	public Obstacle(float x, float y, int type, float deviation ) {
		super(x, y, Constants.OBSTACLE_WIDTH_TYPE_1, Constants.OBSTACLE_HEIGHT_TYPE_1, type);
		this.stateTime = 0;
		myDeviation = deviation;
		velocity.x = Constants.FLOOR_VELOCITY;
	}
	
	/**
	 * Suelo que contiene textura y posición
	 * @param x posición de coordenadas del obstáculo
	 * @param y posición de ordenadas del obstáculo
	 * @param width anchura del obstáculo
	 * @param height altura del obstáculo
	 * @param type constante tipo de obstáculo para su textura
	 * @param deviation
	 */
	public Obstacle(float x, float y, float width, float height, int type, Floor floor, float deviation ) {
		super(x, y, width, height, type);
		myFloor = floor;
		myDeviation = deviation;
		this.stateTime = 0;
		velocity.x = Constants.FLOOR_VELOCITY;
	}
	/**
	 * Actualiza estado del obstáculo
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	public void update(float deltaTime) {
		position.add(-velocity.x * deltaTime, 0);
		bounds.x = position.x - bounds.width / 2.0f;
		bounds.y = position.y - bounds.height / 2.0f;
		stateTime += deltaTime;
	}
	/**
	 * Actualiza estado del obstáculo
	 * 
	 */
	public void update()
	{
		position.x = myFloor.position.x + myDeviation;
		position.y = myFloor.position.y + myFloor.bounds.height/2f + Constants.OBSTACLE_HEIGHT_TYPE_1/2f - 0.2f;
		bounds.x = position.x - bounds.width / 2.0f;
		bounds.y = position.y - bounds.height / 2.0f;
	}
}
