package com.me.cabalga.objects;

import com.me.cabalga.game.DynamicGameObject;
import com.me.cabalga.world.Constants;

/**
 * Clase objeto Pájaro
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class Bird extends DynamicGameObject{

	public float stateTime;
	
	/**
	 * Pájaro que contiene textura y posición
	 * @param x posición de coordenadas del Pájaro
	 * @param y posición de ordenadas del Pájaro
	 * @param type constante tipo de Pájaro para su textura
	 */
	public Bird(float x, float y, int type) {
		super(x, y, Constants.BIRD_WIDTH, Constants.BIRD_HEIGHT - Constants.BIRD_PAINT_HEIGHT*1.5f, type);
		this.stateTime = 0;
		velocity.x = Constants.BIRD_VELOCITY;
	}
	
	/**
	 * Pájaro que contiene textura y posición
	 * @param x posición de coordenadas del Pájaro
	 * @param y posición de ordenadas del Pájaro
	 * @param width anchura del Pájaro
	 * @param height altura del Pájaro
	 * @param type constante tipo de Pájaro para su textura
	 */
	public Bird(float x, float y, float width, float height, int type) {
		super(x, y, width, height, type);
		this.stateTime = 0;
		velocity.x = Constants.BIRD_VELOCITY;
	}
	
	/**
	 * Actualiza estado del Pájaro
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	public void update(float deltaTime) {
		position.add(-velocity.x * deltaTime, 0);
		bounds.x = position.x - bounds.width / 2.0f;
		bounds.y = position.y - bounds.height / 2.0f + Constants.BIRD_PAINT_HEIGHT;
		stateTime += deltaTime;
	}
}
