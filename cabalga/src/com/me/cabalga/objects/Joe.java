package com.me.cabalga.objects;

import com.me.cabalga.game.DynamicGameObject;
import com.me.cabalga.world.Constants;
import com.me.cabalga.world.World;

/**
 * Clase objeto personaje
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class Joe extends DynamicGameObject
{
	public int state;
	public float stateTime;
	
	/**
	 * Personaje que contiene textura y posición
	 * @param x posición de coordenadas del personaje
	 * @param y posición de ordenadas del personaje
	 * @param type constante tipo de personaje para su textura
	 */
	public Joe (float x, float y, int type ) {
		super(x, y, Constants.HORSE_WIDTH - Constants.HORSE_PAINT_WIDTH, Constants.HORSE_HEIGHT, type);
		state = Constants.HORSE_STATE_HIT;
		stateTime = 0;
	}
	
	/**
	 * Actualiza estado del personaje
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 * @param jump indica si se ha pulsado para que el personaje salte
	 */
public void update (float deltaTime, boolean jump) {
		
		if (jump && state == Constants.HORSE_STATE_HIT){
			jump();
		}
		if (!jump && state == Constants.HORSE_STATE_JUMP){
			fall();
		}
				
		if (jump && velocity.y != 0 && state != Constants.HORSE_STATE_HIT){
			velocity.add(World.gravity.x, World.gravity.y * deltaTime);
			position.add(velocity.x, velocity.y * deltaTime);
			bounds.x = (position.x - Constants.HORSE_WIDTH / 2) /*+ Constants.HORSE_PAINT_WIDTH*2*/;
			bounds.y = position.y - Constants.HORSE_HEIGHT / 2;
			stateTime = 0;
			state = Constants.HORSE_STATE_JUMP;
		}
		else if( velocity.y < 0 && state == Constants.HORSE_STATE_FALL){
			velocity.add(World.gravity.x, World.gravity.y * deltaTime);
			position.add(velocity.x, velocity.y *deltaTime);
			bounds.x = (position.x - Constants.HORSE_WIDTH / 2) /*+ Constants.HORSE_PAINT_WIDTH*2*/;
			bounds.y = position.y - Constants.HORSE_HEIGHT / 2;
			stateTime = 0;
		}
		stateTime += deltaTime;
	}

	/**
	 * Actualiza la velocidad y el estado del personaje al saltar
	 *
	 */
	public void jump(){
		velocity.y = Constants.HORSE_JUMP_VELOCITY;
		state = Constants.HORSE_STATE_JUMP;
	}
	
	/**
	 * Actualiza la velocidad y el estado del personaje al haber colision con el suelo
	 *
	 */
	public void hitPlatform () {
		velocity.y = 0;		
		state = Constants.HORSE_STATE_HIT;
		
	}
	
	/**
	 * Actualiza la velocidad y el estado del personaje al caer del salto
	 *
	 */
	public void fall()
	{	
		if(velocity.y > -2f)
			velocity.y = -2f;
		state = Constants.HORSE_STATE_FALL;
		stateTime = 0;
	}
}
