package com.me.cabalga.objects;

import com.me.cabalga.game.DynamicGameObject;
import com.me.cabalga.world.Constants;

/**
 * Clase objeto suelo
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class Floor extends DynamicGameObject{
	
	float stateTime;
	Floor previousFloor;

	/**
	 * Suelo que contiene textura y posición
	 * @param x posición de coordenadas del suelo
	 * @param y posición de ordenadas del suelo
	 * @param type constante tipo de suelo para su textura
	 */
	public Floor(float x, float y, int type){
		super(x, y, Constants.FLOOR_WIDTH_SHORT, Constants.FLOOR_HEIGHT_LOW, type);
		this.stateTime = 0;
		velocity.x = Constants.FLOOR_VELOCITY;
		
	}
	
	/**
	 * Suelo que contiene textura y posición
	 * @param x posición de coordenadas del suelo
	 * @param y posición de ordenadas del suelo
	 * @param width anchura del suelo
	 * @param height altura del suelo
	 * @param type constante tipo de suelo para su textura
	 */
	public Floor(float x, float y, float width, float height, int type){
		super(x, y, width, height, type);
		this.stateTime = 0;
		velocity.x = Constants.FLOOR_VELOCITY;
		
	}
	
	/**
	 * Actualiza estado del suelo
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	public void update(float deltaTime){
		velocity.add(Constants.FLOOR_ACELERATION * deltaTime, 0);
		position.add(-velocity.x * deltaTime, 0);
		bounds.x = position.x - bounds.width / 2.0f;
		bounds.y = position.y - bounds.height / 2.0f;
		stateTime += deltaTime;
	}
}
