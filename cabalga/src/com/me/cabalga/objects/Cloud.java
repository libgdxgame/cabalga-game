package com.me.cabalga.objects;

import com.me.cabalga.game.DynamicGameObject;
import com.me.cabalga.world.Constants;

/**
 * Clase objeto nube
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class Cloud extends DynamicGameObject{
	float stateTime;

	/**
	 * Nube que contiene textura y posición
	 * @param type constante tipo de nube para su textura
	 * @param x posición de coordenadas de la nube
	 * @param y posición de ordenadas de la nube
	 */
	public Cloud(int type, float x, float y){
		super(x, y, Constants.CLOUD_WIDTH, Constants.CLOUD_HEIGHT, type);
		this.stateTime = 0;
		if(type == Constants.CLOUD_TYPE_1)
			velocity.x = Constants.CLOUD_VELOCITY_1;
		else velocity.x = Constants.CLOUD_VELOCITY_2;
		
	}
	
	/**
	 * Con él podemos actualizar el estado de la nube
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	public void update(float deltaTime){
		position.add(-velocity.x * deltaTime, 0);
		bounds.x = position.x - bounds.width / 2f;
		bounds.y = position.y - bounds.height / 2f;
		stateTime += deltaTime;
	}
}
