package com.me.cabalga.world;

import com.badlogic.gdx.Gdx;


/**
 * Clase que contiene la mayoría de constantes del juego
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class Constants
{
	// Visible game world is 5 meters wide
	public static final float VIEWPORT_WIDTH = 15;
	// Visible game world is 5 meters tall
	public static final float VIEWPORT_HEIGHT = 10;
	
	public static final float VIEWPORT_WIDTH_BOUNDS = VIEWPORT_WIDTH/2f;
	public static final float VIEWPORT_HEIGHT_BOUNDS = VIEWPORT_HEIGHT/2f;
	
	// Screen Width & Heigth
	public static final float WIDHT = Gdx.graphics.getWidth();
	public static final float HEIGTH = Gdx.graphics.getHeight();
	
	
	/************************************************************************
	 * 								ANIMATION								*
	 ************************************************************************/
	public static final int ANIMATION_LOOPING = 0;
	public static final int ANIMATION_NONLOOPING = 1;
	
	
	
	/************************************************************************
	 * 								GAME									*
	 ************************************************************************/
	public static final int GAME_READY = 0;
	public static final int GAME_RUNNING = 1;
	public static final int GAME_PAUSED = 2;
	public static final int GAME_OVER = 4;
	
	
	/************************************************************************
	 * 								WORLD									*
	 ************************************************************************/
	public static final int WORLD_WIDTH = 15;
	public static final int WORLD_HEIGHT = 10;
	
	public static final int WORLD_STATE_RUNNING = 0;
	public static final int WORLD_STATE_NEXT_LEVEL = 1;
	public static final int WORLD_STATE_GAME_OVER = 2;
	
	public static final int MAX_NUM_BLOQUES = 5;
	public static final int MAX_NUM_OBSTACLES = 5;
	public static final int MAX_NUM_BIRD = 5;
	public static final int MAX_NUM_CLOUDS = 10;
	
	
	/************************************************************************
	 * 								FLOOR								*
	 ************************************************************************/
	public static final float FLOOR_WIDTH_SHORT = 4;
	public static final float FLOOR_WIDTH_LARGE = 8;
	public static final float FLOOR_HEIGHT_LOW = 1.5f;
	public static final float FLOOR_HEIGHT_HIGHT = 3;
	
	public static final int FLOOR_TYPE_STATIC = 0;
	public static final int FLOOR_TYPE_MOVING = 1;
	public static final int FLOOR_VELOCITY = 3;
	public static final float FLOOR_ACELERATION = 0.01f;
	
	public static final int FLOOR_TYPE_1 = 1;
	public static final int FLOOR_TYPE_2 = 2;
	public static final int FLOOR_TYPE_3 = 3;
	public static final int FLOOR_TYPE_4 = 4;
	
	/************************************************************************
	 * 								OBSTACLE								*
	 ************************************************************************/
	public static final int OBSTACLE_WIDTH_TYPE_1 = 1;
	public static final int OBSTACLE_HEIGHT_TYPE_1 = 1;
	
	public static final float OBSTACLE_WIDTH_TYPE_2 = 1.3f;
	public static final float OBSTACLE_HEIGHT_TYPE_2 = 1.5f;
	
	public static final int OBSTACLE_TYPE_1 = 1;
	public static final int OBSTACLE_TYPE_2 = 2;
	public static final int OBSTACLE_TYPE_3 = 3;
	public static final int OBSTACLE_TYPE_4 = 4;
	public static final int OBSTACLE_TYPE_5 = 5;
	
	/************************************************************************
	 * 								BIRD								*
	 ************************************************************************/
	public static final float BIRD_WIDTH = 1;
	public static final float BIRD_HEIGHT = 0.8f;
	
	public static final int BIRD_VELOCITY = 4;
	
	public static final float BIRD_PAINT_WIDTH = 0.2f;
	public static final float BIRD_PAINT_HEIGHT = 0.2f;
	
	/************************************************************************
	 * 								HORSE									*
	 ************************************************************************/
	
	public static final int HORSE_STATE_STOPPED = 0;
	public static final int HORSE_STATE_JUMP = 1;
	public static final int HORSE_STATE_FALL = 2;
	public static final int HORSE_STATE_HIT = 3;
	public static final int HORSE_JUMP_VELOCITY = 8;
	public static final int HORSE_MOVE_VELOCITY = 20;
	public static final int HORSE_WIDTH = 1;
	public static final int HORSE_HEIGHT = 2;
	
	public static final float HORSE_PAINT_WIDTH = 0.4f;
	public static final float HORSE_PAINT_HEIGHT = 0.1f;
	
	public static final int HORSE_TYPE_1 = 1;
	
	
	/************************************************************************
	 * 								CLOUD								*
	 ************************************************************************/
	public static final float CLOUD_WIDTH = 4.0f;
	public static final float CLOUD_HEIGHT = 2.0f;
	
	public static final int CLOUD_TYPE_1 = 1;
	public static final int CLOUD_TYPE_2 = 2;
	
	public static final float CLOUD_VELOCITY_1 = 0.5f;
	public static final float CLOUD_VELOCITY_2 = 0.3f;
	
}
