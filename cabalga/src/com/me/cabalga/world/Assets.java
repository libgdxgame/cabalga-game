package com.me.cabalga.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.me.cabalga.game.Settings;

/**
 * Clase donde se registran todos los recursos del sistema, tanto audio como texturas
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class Assets {

	public static Texture background;
	public static TextureRegion backgroundRegion;
	
	public static Texture items;
	public static Texture joe;
	public static Texture bird;

	public static TextureRegion joeRegion[];
	public static TextureRegion birdRegion[];
	public static TextureRegion[] floorsRegion = new TextureRegion[10];
	public static TextureRegion[] obstaclesRegion = new TextureRegion[10];
	public static TextureRegion[] cloudsRegion = new TextureRegion[10];
		
	public static TextureRegion transparency;
	public static TextureRegion soundOn;
	public static TextureRegion soundOff;
	public static TextureRegion pause;
	
	public static TextureRegion play;
	public static TextureRegion scores;
	public static TextureRegion restart;
	public static TextureRegion home;
	public static TextureRegion quit;
	
	
	public static Animation joeAnimation;	
	public static Animation birdAnimation;	
	
	public static Music music;
	public static Sound clickSound;
	public static Sound fall;
	
	public static BitmapFont font;
	public static BitmapFont tittle;

	public static final int FRAME_COLS_JOE = 4;   
	public static final int FRAME_COLS_BIRD = 3; 
    public static final int FRAME_ROWS = 2; 
	
    /**
     * Antes de utilizar una textura la cargamos en memoria
     * @param file cadena donde se aloja el recurso
     * @return Texture
     */
	public static Texture loadTexture (String file) {
		return new Texture(Gdx.files.internal(file));
	}
	
	/**
	 * Precargamos todos las texturas necesarias para el juego, llamados proxys
	 */
	public static void load ()
	{
		background = loadTexture("data/background.png");
		backgroundRegion = new TextureRegion(background, 0, 0, 1920, 1080);
		
		items = loadTexture("data/items-2.png");
		
		joe = loadTexture("data/animacion2.png");
		
		bird = loadTexture("data/birdanimation.png");
		
		////////// ANIMATION /////////////
		
		TextureRegion[][] tmpjoe = TextureRegion.split(joe, 824 / FRAME_COLS_JOE, 748 / FRAME_ROWS);	
		joeRegion = new TextureRegion[FRAME_COLS_JOE * FRAME_ROWS];
		
		TextureRegion[][] tmpbird = TextureRegion.split(bird, 844 / FRAME_COLS_BIRD, 500 / FRAME_ROWS);	
		birdRegion = new TextureRegion[FRAME_COLS_BIRD * FRAME_ROWS];
		
		int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
                for (int j = 0; j < FRAME_COLS_JOE; j++) {
                	joeRegion[index++] = tmpjoe[i][j];
                	
                }
        }
        
        index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
                for (int j = 0; j < FRAME_COLS_BIRD; j++) {
                	birdRegion[index++] = tmpbird[i][j];
                	
                }
        }
        
        joeAnimation = new Animation(0.07f, joeRegion);
        birdAnimation = new Animation(0.08f, birdRegion);
        
        //////////////////////////////////
		
        floorsRegion[Constants.FLOOR_TYPE_1] = new TextureRegion(items, 30, 383, 519, 387);
        floorsRegion[Constants.FLOOR_TYPE_2] = new TextureRegion(items, 30, 383, 519, 187);
        floorsRegion[Constants.FLOOR_TYPE_3] = new TextureRegion(items, 570, 383, 1090, 387);
        floorsRegion[Constants.FLOOR_TYPE_4] = new TextureRegion(items, 570, 383, 1090, 187);
        
        obstaclesRegion[Constants.OBSTACLE_TYPE_1] = new TextureRegion(items, 1698, 315, 1960 - 1698, 545 - 315);
        obstaclesRegion[Constants.OBSTACLE_TYPE_2] = new TextureRegion(items, 1571, 1113, 1860 - 1571, 1365 - 1113);
        
        cloudsRegion[Constants.CLOUD_TYPE_1] = new TextureRegion(items, 0, 0, 603, 354);
        cloudsRegion[Constants.CLOUD_TYPE_2] = new TextureRegion(items, 640, 0, 540, 270);
        
        transparency = new TextureRegion(items, 88, 1404, 896 - 88, 1948 - 1404);
		
		soundOff = new TextureRegion(items, 1922, 49, 121, 109);
		soundOn = new TextureRegion(items, 1922, 165, 121, 275 - 166);
		pause = new TextureRegion(items, 1800, 166, 1918 - 1800, 275 - 166 );
		
		play = new TextureRegion(items, 1680, 166, 121, 275 - 166);
		scores = new TextureRegion(items, 1323, 166, 110, 275 - 166);
		quit = new TextureRegion(items, 1800, 49, 1918 - 1800, 109);
		home = new TextureRegion(items, 1450, 166, 1555 - 1450, 275 - 166);
		restart = new TextureRegion(items, 1564, 166, 1672 - 1564, 275 - 166);
				
		font = new BitmapFont(Gdx.files.internal("data/font.fnt"), Gdx.files.internal("data/font.png"), false);
		tittle = new BitmapFont(Gdx.files.internal("data/tittle.fnt"), Gdx.files.internal("data/tittle.png"), false);
		
		
		music = Gdx.audio.newMusic(Gdx.files.internal("data/music.mp3"));
		music.setLooping(true);
		music.setVolume(0.5f);
		if (Settings.soundEnabled) music.play();
		
		clickSound = Gdx.audio.newSound(Gdx.files.internal("data/click.mp3"));
		
		fall = Gdx.audio.newSound(Gdx.files.internal("data/fall.mp3"));
		
	}
	
	/**
	 * Sirve para hacer sonar una pista
	 * @param sound parámetro de tipo Sound
	 */
	public static void playSound (Sound sound) {
		if (Settings.soundEnabled) sound.play(1);
	}
	
	/**
	 * Nos indica el tipo de suelo que queremos cargar
	 * @param type constante que indica el tipo de textura
	 * @return TextureRegion
	 */
	public static TextureRegion  getFloorType( int type )
	{
		if(type < floorsRegion.length)
			return floorsRegion[type];
		else
			return null;
	}
	
	/**
	 * Nos indica el tipo de obstáculo que queremos cargar
	 * @param type constante que indica el tipo de textura
	 * @return TextureRegion
	 */
	public static TextureRegion  getObstacleType( int type )
	{
		if(type < obstaclesRegion.length)
			return obstaclesRegion[type];
		else
			return null;
	}
	
	/**
	 * Nos indica el tipo de nube que queremos cargar
	 * @param type constante que indica el tipo de textura
	 * @return TextureRegion
	 */
	public static TextureRegion  getCloudType( int type )
	{
		if(type < cloudsRegion.length)
			return cloudsRegion[type];
		else
			return null;
	}
}
