package com.me.cabalga.world;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Superclase utilizada para animar objetos
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class Animation
{

	final TextureRegion[] keyFrames;
	final float frameDuration;

	/**
	 * Soporta un número ilimitado de frames clave de tipo TextureRegion
	 * @param frameDuration
	 * @param keyFrames
	 */
	public Animation (float frameDuration, TextureRegion... keyFrames)
	{
		this.frameDuration = frameDuration;
		this.keyFrames = keyFrames;
	}

	/**
	 * Nos devuelve un frame clave dependiendo de la actualización del juego
	 * @param stateTime
	 * @param mode
	 * @return TextureRegion
	 */
	public TextureRegion getKeyFrame (float stateTime, int mode)
	{
		int frameNumber = (int)(stateTime / frameDuration);

		if (mode == Constants.ANIMATION_NONLOOPING) {
			frameNumber = Math.min(keyFrames.length - 1, frameNumber);
		} else {
			frameNumber = frameNumber % keyFrames.length;
		}
		return keyFrames[frameNumber];
	}
}
