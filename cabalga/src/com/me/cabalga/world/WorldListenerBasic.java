package com.me.cabalga.world;

import com.me.cabalga.world.WorldListener;

/**
 * Clase que se encarga de los sonidos del juego
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 */
public class WorldListenerBasic implements WorldListener
{
	/**
	 * Asigna sonido al saltar
	 */
	@Override
	public void jump () {
		Assets.playSound(Assets.clickSound);
	}
	/**
	 * Asigna sonido al colisionar
	 */
		@Override
	public void fall () {
		Assets.playSound(Assets.fall);
	}
	
}
