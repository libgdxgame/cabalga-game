package com.me.cabalga.world;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.me.cabalga.objects.Bird;
import com.me.cabalga.objects.Cloud;
import com.me.cabalga.objects.Floor;
import com.me.cabalga.objects.Joe;
import com.me.cabalga.objects.Obstacle;
import com.me.cabalga.world.WorldListener;

/**
 * Clase que se encarga de la lógica de aparición de los objetos del mundo 
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class World{
	public static final Vector2 gravity = new Vector2(0, -10);
	public final WorldListener listener;
	public final Joe joe;
	public final List<Floor> floors;
	public final List<Obstacle> obstacles;
	public final List<Bird> birds;
	public final List<Cloud> clouds;
	public float score;
	public int state;
	float distance;
	float distance_cloud;
	boolean first_floor;
	
	/**
	 * Mundo que contiene jugador, suelo, obstáculos y nubes, 
	 * @param listener parámetro de tipo WorldListener
	 */
	public World (WorldListener listener) {
		this.joe = new Joe(3f, 5, Constants.HORSE_TYPE_1);
		this.floors = new ArrayList<Floor>();
		this.obstacles = new ArrayList<Obstacle>();
		this.birds = new ArrayList<Bird>();
		this.clouds = new ArrayList<Cloud>();
		this.listener = listener;
		this.distance = 0;
		this.distance_cloud = 0;
		this.score = 0;
		this.state = Constants.WORLD_STATE_RUNNING;
		this.first_floor = true;
		
		addFloor();
		addCloud();
	}
	
	/**
	 * Actualiza estado del mundo
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	public void update (float deltaTime, boolean input) {
		updateJoe(deltaTime, input);
		updateFloor(deltaTime);
		updateObstacle(deltaTime);
		updateBird(deltaTime);
		updateCloud(deltaTime);
		checkCollisions();
		score += Constants.FLOOR_VELOCITY * deltaTime + Constants.FLOOR_VELOCITY * Constants.FLOOR_ACELERATION * deltaTime;
	}
	/**
	 * Se encarga de la lógica de aparición de suelo
	 */
	public void addFloor() {
		float farwest = 0;
		Floor farwestFloor = null, prevFloor;
		
		while(floors.size() <= Constants.MAX_NUM_BLOQUES){
			int tam = floors.size();
			if(tam>0)
			{
				Iterator<Floor> it = floors.iterator();
				
				while(it.hasNext())
				{
					prevFloor = it.next();
					if(farwest<=prevFloor.position.x)
					{
						farwest = prevFloor.position.x;
						farwestFloor = prevFloor;
					}
				}
				distance = farwestFloor.position.x + farwestFloor.bounds.width/2f;
				Floor floor = createFloor(MathUtils.random(Constants.FLOOR_TYPE_1,Constants.FLOOR_TYPE_4), distance + MathUtils.random(0,3) );
				floors.add(floor);
				addObstacle(floor);
			}
			else{
				distance = 0f;
				Floor floor = createFloor(Constants.FLOOR_TYPE_4, distance + MathUtils.random(0,3) );
				floors.add(floor);
			}
		}
	}
	/**
	 * Añade un suelo con una longitud
	 * @param type constante tipo de suelo para su textura
	 * @param distancia parámetro para calcular la longitud del suelo
	 */
	public Floor createFloor(int type, float distancia ){
		Floor floor;
		
		if(type == Constants.FLOOR_TYPE_2)
			floor = new Floor(distancia + Constants.FLOOR_WIDTH_SHORT/2f, Constants.FLOOR_HEIGHT_LOW/2f, Constants.FLOOR_WIDTH_SHORT, Constants.FLOOR_HEIGHT_LOW, Constants.FLOOR_TYPE_2 );
		else if(type == Constants.FLOOR_TYPE_3)
			floor = new Floor(distancia + Constants.FLOOR_WIDTH_LARGE/2f, Constants.FLOOR_HEIGHT_HIGHT/2f, Constants.FLOOR_WIDTH_LARGE, Constants.FLOOR_HEIGHT_HIGHT, Constants.FLOOR_TYPE_3 );
		else if(type == Constants.FLOOR_TYPE_4)
			floor = new Floor(distancia + Constants.FLOOR_WIDTH_LARGE/2f, Constants.FLOOR_HEIGHT_LOW/2f, Constants.FLOOR_WIDTH_LARGE, Constants.FLOOR_HEIGHT_LOW, Constants.FLOOR_TYPE_4 );
		else
			floor = new Floor(distancia + Constants.FLOOR_WIDTH_SHORT/2f, Constants.FLOOR_HEIGHT_HIGHT/2f, Constants.FLOOR_WIDTH_SHORT, Constants.FLOOR_HEIGHT_HIGHT, Constants.FLOOR_TYPE_1 );
		
		return floor;
	}
	/**
	 * Se encarga de la lógica de aparición de obstáculos
	 */
	public void addObstacle(Floor floor) {
			float random = MathUtils.random(1,23);
			
			if(obstacles.size() <= Constants.MAX_NUM_OBSTACLES &&  ((10 < random) && (23 > random))){
//				float widhtFloor = floor.bounds.width;
//				float deviation = MathUtils.random(-widhtFloor/2f + Constants.OBSTACLE_HEIGHT_TYPE_1, widhtFloor/2f - Constants.OBSTACLE_HEIGHT_TYPE_1);
//				obstacles.add(new Obstacle(floor.position.x, floor.position.y + floor.bounds.height/2f + Constants.OBSTACLE_HEIGHT_TYPE_1/2f - 0.2f, Constants.OBSTACLE_WIDTH_TYPE_1, Constants.OBSTACLE_HEIGHT_TYPE_1, Constants.OBSTACLE_TYPE_1, floor, deviation ));
				Obstacle obstacle = createObstacle(MathUtils.random(Constants.OBSTACLE_TYPE_1,Constants.OBSTACLE_TYPE_3), floor);
				obstacles.add(obstacle);
			}
			if ((1 < random) && (10 > random)) {
				while(birds.size() <= Constants.MAX_NUM_BIRD){
					Bird bird = createBird(floor);
					birds.add(bird);
				}
			}
	}
	/**
	 * Añade un obstáculo
	 * @param type constante tipo de obstáculo para su textura
	 * @param floor parámetro para la posición x del obstaculo
	 */
	public Obstacle createObstacle(int type, Floor floor ){
		Obstacle obstacle;
		
		if(type == Constants.OBSTACLE_TYPE_2){
			float widhtFloor = floor.bounds.width;
			float deviation = MathUtils.random(-widhtFloor/2f + Constants.OBSTACLE_HEIGHT_TYPE_2, widhtFloor/2f - Constants.OBSTACLE_HEIGHT_TYPE_2);
			obstacle = new Obstacle(floor.position.x, floor.position.y + floor.bounds.height/2f + Constants.OBSTACLE_HEIGHT_TYPE_2/2f - 0.2f, Constants.OBSTACLE_WIDTH_TYPE_2, Constants.OBSTACLE_HEIGHT_TYPE_2, Constants.OBSTACLE_TYPE_2, floor, deviation );
		}
		else{
			float widhtFloor = floor.bounds.width;
			float deviation = MathUtils.random(-widhtFloor/2f + Constants.OBSTACLE_HEIGHT_TYPE_1, widhtFloor/2f - Constants.OBSTACLE_HEIGHT_TYPE_1);
			obstacle = new Obstacle(floor.position.x, floor.position.y + floor.bounds.height/2f + Constants.OBSTACLE_HEIGHT_TYPE_1/2f - 0.2f, Constants.OBSTACLE_WIDTH_TYPE_1, Constants.OBSTACLE_HEIGHT_TYPE_1, Constants.OBSTACLE_TYPE_1, floor, deviation );
		}
		return obstacle;
	}
	
	public Bird createBird(Floor floor ){
		Bird bird;
		
		bird = new Bird(floor.position.x + MathUtils.random(0.0f,2.0f), 7 + MathUtils.random(0.0f,2.0f), Constants.BIRD_WIDTH, Constants.BIRD_WIDTH, 1);
		return bird;
	}
	

	/**
	 * Se encarga de cargar y de la lógica de las nubes 
	 */
	public void addCloud() {
		while(clouds.size() <= Constants.MAX_NUM_CLOUDS){
			Cloud cloud = new Cloud(MathUtils.random(Constants.CLOUD_TYPE_1, Constants.CLOUD_TYPE_2), distance_cloud, MathUtils.random(6, 9));
			clouds.add(cloud);
			distance_cloud += MathUtils.random(4,7);
		}
	}
	/**
	 * Actualiza el estado del suelo
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	private void updateFloor (float deltaTime) {
		int len = floors.size();
		
		for(int i=0;i<len;i++){
			Floor floor = floors.get(i);
			floor.update(deltaTime);
			if(floor.position.x <= -floor.bounds.width){
				floors.remove(floor);
				addFloor();
				len = floors.size();
			}
		}
	}
	/**
	 * Actualiza el estado de los obstáculos
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	private void updateObstacle (float deltaTime) {
		int len = obstacles.size();
		
		for(int i=0;i<len;i++){
			Obstacle obstacle = obstacles.get(i);
			obstacle.update();
			if(obstacle.position.x <= -obstacle.bounds.width){
				obstacles.remove(obstacle);
				len = obstacles.size();
			}
		}
	}
	
	private void updateBird (float deltaTime) {
		int len = birds.size();
		
		for(int i=0;i<len;i++){
			Bird bird = birds.get(i);
			bird.update(deltaTime);
			if(bird.position.x <= -bird.bounds.width-3){
				birds.remove(bird);
				len = birds.size();
			}
		}
	}
	/**
	 * Actualiza el estado de las nubes
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	private void updateCloud (float deltaTime) {
		int len = clouds.size();
		
		for(int i=0;i<len;i++){
			Cloud cloud = clouds.get(i);
			cloud.update(deltaTime);
			if(cloud.position.x <= -2){
				addCloud();
				clouds.remove(cloud);
				len = clouds.size();
			}
		}
	}
	/**
	 * Actualiza el personaje
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 * @param touch indica si se ha pulsado para que el personaje salte
	 */
	private void updateJoe (float deltaTime, boolean touch) {
		if (joe.position.y < -1) state = Constants.WORLD_STATE_GAME_OVER;
		joe.update(deltaTime, touch);
		
	}
	/**
	 * Detecta colisiones en el mundo
	 */
	private void checkCollisions () {
		boolean suelo = false;
		boolean obstaculo = false;
		
		suelo = checkFloorCollisions();
		obstaculo = checkObstacleCollisions();
		checkBirdCollisions();
		if(!suelo && !obstaculo)
			if(joe.state != Constants.HORSE_STATE_JUMP && joe.state != Constants.HORSE_STATE_FALL)
				joe.fall();
	
	}
	/**
	 * Detecta colisiones entre el personaje y el suelo
	 */
	private boolean checkFloorCollisions () {
		int len = floors.size();
		boolean enc = false;
		
		for(int i=0;i<len;i++){
			Floor floor = floors.get(i);
			if (Intersector.overlaps(joe.bounds, floor.bounds)) {
				if(joe.bounds.y + 0.2f  < floor.position.y + (floor.position.y - floor.bounds.y) ){
					state = Constants.WORLD_STATE_GAME_OVER;
					enc = true;
					break;
				}
				joe.hitPlatform();
				enc = true;
			}
		}
		return enc;
	}
	/**
	 * Detecta colisiones entre el personaje y los obstáculos
	 */
	private boolean checkObstacleCollisions () {
		int n = obstacles.size();
		boolean enc = false;
		
		for(int i=0;i<n;i++){
			Obstacle obstacle = obstacles.get(i);
			if (Intersector.overlaps(joe.bounds, obstacle.bounds)) {
				if(joe.bounds.y + 0.2f  < obstacle.position.y + (obstacle.position.y - obstacle.bounds.y) ){
					state = Constants.WORLD_STATE_GAME_OVER;
					enc = true;
					break;
				}
				joe.hitPlatform();
				enc = true;
			}
		}
		return enc;
	}
	
	private boolean checkBirdCollisions () {
		int n = birds.size();
		boolean enc = false;
		
		for(int i=0;i<n;i++){
			Bird bird = birds.get(i);
			if (Intersector.overlaps(joe.bounds, bird.bounds)) {
					state = Constants.WORLD_STATE_GAME_OVER;
					enc = true;
			}
		}
		return enc;
	}
}
