package com.me.cabalga.world;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.me.cabalga.objects.Bird;
import com.me.cabalga.objects.Cloud;
import com.me.cabalga.objects.Floor;
import com.me.cabalga.objects.Obstacle;
/**
 * Clase que se encarga de renderizar los objetos del mundo
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class WorldRenderer {	
	World world;
	OrthographicCamera cam;
	SpriteBatch batch;
	TextureRegion background;
	float stateTime;

	Animation horseRun;
	/**
	 * Constructor para la cámara del mundo
	 * @param batch parámetro de tipo SpriteBatch
	 * @param world parámetro de tipo World
	 */
	public WorldRenderer (SpriteBatch batch, World world) {
		this.world = world;
		this.cam = new OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
		this.cam.position.set(Constants.VIEWPORT_WIDTH_BOUNDS, Constants.VIEWPORT_HEIGHT_BOUNDS, 0);
		this.batch = batch;
	}
	/**
	 * Se encarga de inicializar el renderizado del mundo
	 */
	public void render () {
		cam.update();
		batch.setProjectionMatrix(cam.combined);
		renderBackground();
		renderObjects();
	}
	/**
	 * Se encarga de renderizar el fondo del mundo
	 */
	public void renderBackground () {
		batch.disableBlending();
		batch.begin();
		batch.draw(Assets.backgroundRegion, cam.position.x - Constants.VIEWPORT_WIDTH_BOUNDS, cam.position.y - Constants.VIEWPORT_HEIGHT_BOUNDS, Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
		batch.end();
	}
	/**
	 * Se encarga de inicilizar el renderizado de objetos
	 */
	public void renderObjects () {
		batch.enableBlending();
		batch.begin();
		renderCloud();
		renderObstacle();
		renderBird();
		renderFloor();
		renderJoe();
		batch.end();
	}
	/**
	 * Se encarga de renderizar el suelo
	 */
	private void renderFloor () {
		TextureRegion keyFrame;
		int len = world.floors.size();
		for(int i=0;i<len;i++){
			Floor floor = world.floors.get(i);
			keyFrame = Assets.getFloorType(floor.type);
			batch.draw(keyFrame, floor.bounds.x, floor.bounds.y, floor.bounds.width, floor.bounds.height);
//			batch.draw(Assets.transparency, floor.bounds.x, floor.bounds.y, floor.bounds.width, floor.bounds.height);
		}
	}
	/**
	 * Se encarga de renderizar los obstáculos
	 */
	private void renderObstacle () {
		TextureRegion keyFrame;
		int n = world.obstacles.size();
		for(int i=0;i<n;i++){
			Obstacle obstacle = world.obstacles.get(i);
			keyFrame = Assets.getObstacleType(obstacle.type);
			batch.draw(keyFrame, obstacle.bounds.x, obstacle.bounds.y, obstacle.bounds.width , obstacle.bounds.height );
//			batch.draw(Assets.transparency, obstacle.bounds.x, obstacle.bounds.y, obstacle.bounds.width , obstacle.bounds.height );
		}		
	}

	/**
	 * Se encarga de renderizar los pájaros
	 */
	private void renderBird () {
		TextureRegion keyFrame;
		int n = world.birds.size();
		for(int i=0;i<n;i++){
			Bird bird = world.birds.get(i);
			keyFrame = Assets.birdAnimation.getKeyFrame(bird.stateTime, Constants.ANIMATION_LOOPING);
			batch.draw(keyFrame, bird.bounds.x , bird.bounds.y - Constants.BIRD_PAINT_HEIGHT , -bird.bounds.width, bird.bounds.height + Constants.BIRD_PAINT_HEIGHT*2);
//			batch.draw(Assets.transparency, bird.bounds.x, bird.bounds.y, -bird.bounds.width, bird.bounds.height);
		}		
	}
	

	/**
	 * Se encarga de renderizar las nubes
	 */
	private void renderCloud () {
		TextureRegion keyFrame;
		int n = world.clouds.size();
		for(int i=0;i<n;i++){
			Cloud cloud = world.clouds.get(i);
			keyFrame = Assets.getCloudType(cloud.type);
			batch.draw(keyFrame, cloud.bounds.x, cloud.bounds.y, cloud.bounds.width, cloud.bounds.height);
		}		
	}
	
	/**
	 * Se encarga de renderizar el personaje
	 */
	private void renderJoe () {
		stateTime = 0;
		TextureRegion keyFrame;            
        keyFrame = Assets.joeAnimation.getKeyFrame(world.joe.stateTime, Constants.ANIMATION_LOOPING);
		batch.draw(keyFrame, world.joe.bounds.x - Constants.HORSE_PAINT_WIDTH /2, world.joe.bounds.y, world.joe.bounds.width + Constants.HORSE_PAINT_WIDTH, world.joe.bounds.height + Constants.HORSE_PAINT_HEIGHT);
//		batch.draw(Assets.transparency, world.joe.bounds.x, world.joe.bounds.y, world.joe.bounds.width , world.joe.bounds.height);
	}
}
