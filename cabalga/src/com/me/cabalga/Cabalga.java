package com.me.cabalga;

import com.badlogic.gdx.graphics.FPSLogger;
import com.me.cabalga.game.DirectedGame;
import com.me.cabalga.screen.MainMenuScreen;
import com.me.cabalga.screen.ScreenTransition;
import com.me.cabalga.screen.ScreenTransitionFade;
import com.me.cabalga.world.Assets;


/**
 * Clase principal del juego que enviamos como argumento a AbstractGameScreen
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */

public class Cabalga extends DirectedGame {
	boolean firstTimeCreate = true;
	FPSLogger fps;
	
	/**
	 * Carga los recursos y crea el menú principal 
	 */
	@Override
	public void create() {
		Assets.load();
		setScreen(new MainMenuScreen(this));
		//fps = new FPSLogger();
	}
	
	/**
	 * Libera los recursos del juego
	 */
	@Override
	public void dispose () {
		super.dispose();
	}

	/**
	 * Renderiza el estado juego
	 */
	@Override
	public void render() {
		super.render();
		//fps.log();
	}
	
	/**
	 * Reinicia el estado del juego
	 */
	public void restart() {
		super.dispose();
		ScreenTransition transition = ScreenTransitionFade.init(1f);
		setScreen(new MainMenuScreen(this), transition);
	}
}
