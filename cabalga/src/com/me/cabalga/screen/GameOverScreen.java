package com.me.cabalga.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.me.cabalga.game.DirectedGame;
import com.me.cabalga.game.Settings;
import com.me.cabalga.world.Assets;

/**
 * Clase que extiende de AbstractGameScreen para implementarla
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class GameOverScreen extends AbstractGameScreen{
	
	DirectedGame game;
	
	OrthographicCamera cam;
	
	Rectangle nextBounds;
	Vector3 touchPoint;
	Texture gameOverImage;
	TextureRegion gameOverRegion;
	SpriteBatch batch;
	int lastScore;
	
	/**
	 * En ella se dibuja el marco de la pantalla y se prepara el lienzo para dibujar
	 * @param game parámetro de tipo DirectedGame
	 * @param lastScore parámetro que define la última puntuación
	 */
	public GameOverScreen (DirectedGame game, int lastScore) {
		super(game);
		this.game = game;
		this.lastScore = lastScore;
		
		cam = new OrthographicCamera(1920, 1080);
		cam.position.set(1920 / 2, 1080 / 2, 0);
		batch = new SpriteBatch();
//		soundBounds = new Rectangle(0, 0, 74, 70);
//		scoreBounds = new Rectangle(500, (1080 / 2) - 100, 220, 100);
//		continueBounds = new Rectangle(1050, (1080 / 2) - 100, 350, 100);
//		helpBounds = new Rectangle((1920 / 2) - 325, (1080 / 2) - 200, 650, 100);
		touchPoint = new Vector3();
		
	}
		
	/**
	 * Método de dibujado
	 */
	public void draw(){
		GLCommon gl = Gdx.gl;
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		cam.update();
		batch.setProjectionMatrix(cam.combined);

		batch.disableBlending();
		batch.begin();
		batch.draw(Assets.backgroundRegion, 0, 0, 1920, 1080);
		batch.end();

		batch.enableBlending();
		batch.begin();
		//Assets.tittle.draw(batch, "CaBaLgA", 500, 1080-40);
		Assets.font.draw(batch, (int) lastScore + "", 500, (1080 / 2));
		Assets.font.draw(batch, "CoNtinuE", 1050, (1080 / 2));
//		batch.draw(Assets.logo, 35, 1080-220-40, 1920-100, 220);
//		batch.draw(Assets.mainMenu, (1920 / 2) - 325, (1080 / 2) - 600, 650, 750);
		batch.draw(Settings.soundEnabled ? Assets.soundOn : Assets.soundOff, 0, 0, 74, 70);
		batch.end();
		
	}
	
	/**
	 * Llama a draw
	 * @param delta
	 */
	public void render(float delta) {
		// TODO Auto-generated method stub
		draw();
		
	}

	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	public void show() {
		// TODO Auto-generated method stub
		
	}

	public void hide() {
		// TODO Auto-generated method stub
		
	}

	public void pause() {
		// TODO Auto-generated method stub
		
	}

	public void resume() {
		// TODO Auto-generated method stub
		
	}

	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public InputProcessor getInputProcessor() {
		// TODO Auto-generated method stub
		return null;
	}


	

}
