package com.me.cabalga.screen;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Clase que se encarga de ejecutar el renderizado de transiciones entre pantallas del juego
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public interface ScreenTransition {
	public float getDuration ();
	public void render (SpriteBatch batch, Texture currScreen, Texture nextScreen, float alpha);
}
