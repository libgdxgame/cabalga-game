package com.me.cabalga.screen;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.InputProcessor;
import com.me.cabalga.game.DirectedGame;

/**
 * Clase abstracta de los métodos de visualización: render, resize, show, pause...
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */

public abstract class AbstractGameScreen implements Screen {
	protected DirectedGame game;

	/**
	 * @param game controlador del juego
	 */
	public AbstractGameScreen (DirectedGame game) {
		this.game = game;
	}

	/**
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	public abstract void render (float deltaTime);
	
	/**
	 * @param width anchura del dispositivo
	 * @param height altura del dispositivo
	 */
	public abstract void resize (int width, int height);
	
	/**
	 * Muestra la pantalla de juego
	 */
	public abstract void show ();
	
	/**
	 * Oculta la pantalla de juego
	 */
	public abstract void hide ();
	
	/**
	 * Pausa la pantalla de juego
	 */
	public abstract void pause ();
	
	/**
	 * Procesador de entrada
	 * @return InputProcessor
	 */
	public abstract InputProcessor getInputProcessor ();

	/**
	 * Recupera la pantalla
	 */
	public void resume () {}
	
	/**
	 * Elimina la pantalla
	 */
	public void dispose () {}

}
