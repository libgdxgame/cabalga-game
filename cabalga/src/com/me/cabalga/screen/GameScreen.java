package com.me.cabalga.screen;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import com.me.cabalga.Cabalga;
import com.me.cabalga.game.DirectedGame;
import com.me.cabalga.world.Assets;
import com.me.cabalga.world.Constants;
import com.me.cabalga.world.OverlapTester;
import com.me.cabalga.world.World;
import com.me.cabalga.world.WorldListener;
import com.me.cabalga.world.WorldListenerBasic;
import com.me.cabalga.world.WorldRenderer;
import com.swarmconnect.Swarm;
import com.swarmconnect.SwarmLeaderboard;

/**
 * Clase que controla la pantalla del juego, su estado, el lienzo de dibujado, la cámara y en general todos los objetos de este
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class GameScreen extends AbstractGameScreen {
	
	int state = Constants.GAME_RUNNING;
	
	DirectedGame game;
	
	OrthographicCamera guiCam;
	Vector3 touchPoint;
	SpriteBatch batch; 
	World world;
	WorldListener worldListener;
	WorldRenderer renderer;
	Rectangle pauseBounds;
	Rectangle resumeBounds;
	Rectangle quitBounds;
	Rectangle homeBounds;
	Rectangle restartBounds;
	Rectangle scoresBounds;
	
	int lastScore;
	String scoreString;
	
	ApplicationType appType = Gdx.app.getType();
	
	/**
	 * Clase que maneja los objetos del sistema
	 * @param game parámetro de tipo DirectedGame
	 */
	public GameScreen (DirectedGame game) {
		super(game);
		this.game = game;
		
		guiCam = new OrthographicCamera(1920, 1080);
		guiCam.position.set(1920 / 2, 1080 / 2, 0);
		touchPoint = new Vector3();
		batch = new SpriteBatch();
		worldListener = new WorldListenerBasic() {};
		world = new World(worldListener);
		renderer = new WorldRenderer(batch, world);
		pauseBounds = new Rectangle(1920 - 128, 1080 - 128, 64, 64);
		resumeBounds = new Rectangle((1920 / 2) -500, (1080/2) - 120, 270, 270);
		quitBounds = new Rectangle((1920 /2) + 200 , (1080/2) - 120, 270, 270);
		
		homeBounds = new Rectangle(690, (1080/2) - 150, 145, 145);
		restartBounds =  new Rectangle(858, (1080/2) - 150, 145, 145);
		scoresBounds = new Rectangle(1020, (1080/2) - 150, 150, 145);
		
		lastScore = 0;
		scoreString = "0";
		
		
	}
	
	/**
	 * Actualiza el estado de todo el juego
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	public void update (float deltaTime) {
		if (deltaTime > 0.1f) deltaTime = 0.1f;

		switch (state)
		{
			case Constants.GAME_READY:
				updateReady();
				break;
			case Constants.GAME_RUNNING:
				updateRunning(deltaTime);
				break;
			case Constants.GAME_PAUSED:
				updatePaused();
				break;
			case Constants.GAME_OVER:
				updateGameOver();
				break;
		}
	}
	
	/**
	 * Modifica el estado del juego a rodando!
	 */
	private void updateReady()
	{
			state = Constants.GAME_RUNNING;
	}

	/**
	 * Este método es llamado para atender a los eventos de entrada durante el juego y actualizarlo en consecuencia
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	private void updateRunning (float deltaTime)
	{		
		boolean touch = Gdx.input.isTouched();
		if (touch) {
			guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

			if (OverlapTester.pointInRectangle(pauseBounds, touchPoint.x, touchPoint.y)) {
//				Assets.playSound(Assets.clickSound);
				state = Constants.GAME_PAUSED;
				return;
			}
		}
		
		ApplicationType appType = Gdx.app.getType();
		
		if (appType == ApplicationType.Android || appType == ApplicationType.iOS) {
			world.update(deltaTime, touch);
		} 
		else {
			world.update(deltaTime, Gdx.input.isKeyPressed(Keys.SPACE));
		}
		if (world.score != lastScore) {
			lastScore = (int)world.score;
			scoreString = "" + lastScore;
		}
		if (world.state == Constants.WORLD_STATE_GAME_OVER)
		{
			Assets.playSound(Assets.fall);
			state = Constants.GAME_OVER;
			if (appType == ApplicationType.Android) 
				SwarmLeaderboard.submitScore(14252, (float) lastScore);
		}
	}
	
	/**
	 * Este método se llama si el juego esta en estado pausa
	 */
	private void updatePaused ()
	{
		
			if (Gdx.input.justTouched()) {
				guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

				if (OverlapTester.pointInRectangle(resumeBounds, touchPoint.x, touchPoint.y)) {
//					Assets.playSound(Assets.clickSound);
					state = Constants.GAME_RUNNING;
					return;
				}
				if (OverlapTester.pointInRectangle(quitBounds, touchPoint.x, touchPoint.y)) {
//					Assets.playSound(Assets.clickSound);
					game.setScreen(new MainMenuScreen(game));
					return;
				}
			}
		}

	/**
	 * Este método es llamado si el juego finaliza
	 */
	private void updateGameOver ()
	{
		if (Gdx.input.justTouched()) {
			guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

			if (OverlapTester.pointInRectangle(restartBounds, touchPoint.x, touchPoint.y)) {
				Assets.playSound(Assets.clickSound);
				ScreenTransition transition = ScreenTransitionFade.init(1f);
				((Cabalga)(game)).dispose();
				game.setScreen(new GameScreen(game), transition);
				return;
			}

			if (OverlapTester.pointInRectangle(homeBounds, touchPoint.x, touchPoint.y)) {
				Assets.playSound(Assets.clickSound);
				world = new World(worldListener);
				renderer = new WorldRenderer(batch, world);
				((Cabalga)(game)).dispose();
				game.setScreen(new MainMenuScreen(game));
				return;
			}
			
			if (OverlapTester.pointInRectangle(scoresBounds, touchPoint.x, touchPoint.y)) {
				Assets.playSound(Assets.clickSound);
				if (appType == ApplicationType.Android) 
					Swarm.showLeaderboards();
			}
		}
	}
	
	/**
	 * Este método se encarga en delegar el dibujado
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	public void draw (float deltaTime)
	{
		GLCommon gl = Gdx.gl;
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		renderer.render();
		guiCam.update();
		batch.setProjectionMatrix(guiCam.combined);
		batch.enableBlending();
		batch.begin();
		
		switch (state)
		{
			case Constants.GAME_READY:
				presentReady();
				break;
			case Constants.GAME_RUNNING:
				presentRunning();
				break;
			case Constants.GAME_PAUSED:
				presentPaused();
				break;
			case Constants.GAME_OVER:
				presentGameOver();
				break;
		}
		batch.end();
	}
	
	/**
	 * Este método no está implementado
	 */
	private void presentReady()
	{
		//batch.draw(Assets.ready, 160 - 192 / 2, 240 - 32 / 2, 192, 32);
	}
	
	/**
	 * Este método dibuja el botón de pause y la puntuación
	 */
	private void presentRunning ()
	{
		batch.draw(Assets.pause, 1920 - 128, 1080 - 128, 95, 90);
		Assets.font.draw(batch, scoreString, 50, 1080 - 50);
	}
	
	/**
	 * Este método dibuja el menú de pausa
	 */
	private void presentPaused () {
		batch.draw(Assets.transparency, 0, 0, 1920, 1080);
		batch.draw(Assets.play, (1920/2) - 500, 1080/2 - 120, 268, 240);
		Assets.font.draw(batch, scoreString, 50, 1080 - 50);
		batch.draw(Assets.quit, (1920/2) + 200, 1080/2 - 120, 278, 250);
//		Assets.font.draw(batch, "quit", (1920/2) +250, (1080/2) + 100);
	}
	
	/**
	 * Este método dibuja la pantalla de game over
	 */
	private void presentGameOver ()
	{
		batch.draw(Assets.transparency, 0, 0, 1920, 1080);
		Assets.tittle.draw(batch, "GaMe OvEr", 370, 1080-40);
		Assets.font.draw(batch, lastScore + " points", 685, 1080-380);
		batch.draw(Assets.home, 690, 1080/2 - 150, 140, 140);
		batch.draw(Assets.restart, 858, 1080/2 - 150, 140, 140);
		batch.draw(Assets.scores, 1020, 1080/2 - 150, 140, 140);
	}

	@Override
	public void render(float delta)
	{
		update(delta);
		draw(delta);
	}

	@Override
	public void resize(int width, int height)
	{
		
	}

	@Override
	public void show()
	{
		
	}

	@Override
	public void hide()
	{
		
	}

	@Override
	public void pause()
	{
		if (state == Constants.GAME_RUNNING) state = Constants.GAME_PAUSED;
	}

	@Override
	public void resume()
	{
		if (state == Constants.GAME_PAUSED) state = Constants.GAME_RUNNING;
	}

	@Override
	public void dispose()
	{
		
	}

	@Override
	public InputProcessor getInputProcessor() {
		// TODO Auto-generated method stub
		return Gdx.input.getInputProcessor();
	}

}
