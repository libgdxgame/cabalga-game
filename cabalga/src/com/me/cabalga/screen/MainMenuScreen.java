package com.me.cabalga.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.me.cabalga.game.DirectedGame;
import com.me.cabalga.game.Settings;
import com.me.cabalga.objects.Cloud;
import com.me.cabalga.objects.Floor;
import com.me.cabalga.world.Assets;
import com.me.cabalga.world.Constants;
import com.me.cabalga.world.OverlapTester;
import com.swarmconnect.Swarm;

/**
 * Clase que controla la pantalla principal del juego, su estado, el lienzo de dibujado, la cámara y en general todos los objetos de este
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class MainMenuScreen extends AbstractGameScreen {
	DirectedGame game;
	
	OrthographicCamera guiCam;
	SpriteBatch batch;
	Rectangle soundBounds;
	Rectangle playBounds;
	Rectangle highscoresBounds;
	Vector3 touchPoint;
	Stage stage;
	ApplicationType appType = Gdx.app.getType();
	Floor floor_1;
	Floor floor_2;
	Floor floor_3;
	Floor floor_4;
	Cloud cloud_1;
	Cloud cloud_2;
	Cloud cloud_3;
	Cloud cloud_4;
	
	/**
	 * Clase que maneja los objetos del sistema
	 * @param game parámetro de tipo DirectedGame
	 */
	
	public MainMenuScreen (DirectedGame game) {
		super(game);
		this.game = game;
		guiCam = new OrthographicCamera(1920, 1080);
		guiCam.position.set(1920 / 2, 1080 / 2, 0);
		batch = new SpriteBatch();
		soundBounds = new Rectangle(0, 0, 74, 70);
		playBounds = new Rectangle(695, (1080 / 2) - 100, 270, 270);
		highscoresBounds = new Rectangle(985, (1080 / 2) - 100, 270, 270);
//		helpBounds = new Rectangle((1920 / 2) - 325, (1080 / 2) - 200, 650, 100);
		touchPoint = new Vector3();
		renderObjects();
	}
	
	/**
	 * Actualiza el estado de la pantalla principal del juego
	 * @param deltaTime constituye el tiempo desde la última actualización de la pantalla
	 */
	public void update (float deltaTime) {
		
		if (Gdx.input.justTouched()) {
			guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
			if (OverlapTester.pointInRectangle(playBounds, touchPoint.x, touchPoint.y)) {
				if(Settings.soundEnabled)
					Assets.playSound(Assets.clickSound);
				ScreenTransition transition = ScreenTransitionFade.init(1f);
				game.setScreen(new GameScreen(game), transition);
				return;
			}
			if (OverlapTester.pointInRectangle(highscoresBounds, touchPoint.x, touchPoint.y)) {
				if(Settings.soundEnabled)
					Assets.playSound(Assets.clickSound);
				if (appType == ApplicationType.Android)
					Swarm.showLeaderboards();				
				// -- SWARM END --
				
				return;
			}
			if (OverlapTester.pointInRectangle(soundBounds, touchPoint.x, touchPoint.y)) {
				Settings.soundEnabled = !Settings.soundEnabled;
				if(Settings.soundEnabled){
					Assets.music.play();
				}
				else
					Assets.music.pause();
			}
		}
	}
	
	/**
	 * Este método se encarga en delegar el dibujado
	 * @param deltaTime constituye el tiempo desde la última actualización de la partida
	 */
	public void draw (float deltaTime) {
		GLCommon gl = Gdx.gl;
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		guiCam.update();
		batch.setProjectionMatrix(guiCam.combined);

		batch.disableBlending();
		batch.begin();
		batch.draw(Assets.backgroundRegion, 0, 0, 1920, 1080);
		batch.end();

		batch.enableBlending();
		batch.begin();
		updateObjects();
		Assets.tittle.draw(batch, "CaBaLgA", 500, 1080-40);
		batch.draw(Assets.play, 680, 1080/2 - 120 , 268, 240);
//		Assets.font.draw(batch, "play", 500, (1080 / 2));
//		Assets.font.draw(batch, "scores", 1050, (1080 / 2));
		batch.draw(Assets.scores, 980, 1080/2 -120 , 250, 240);
//		batch.draw(Assets.logo, 35, 1080-220-40, 1920-100, 220);
//		batch.draw(Assets.mainMenu, (1920 / 2) - 325, (1080 / 2) - 600, 650, 750);
		batch.draw(Settings.soundEnabled ? Assets.soundOn : Assets.soundOff, 5, 5, 86, 80);
		batch.end();
	}
	/**
	 * Este método se encarga de dibujar los elementos del fondo en la pantalla principal
	 * 
	 */
	private void renderObjects() {
		floor_1 = new Floor(80, 0, 500, 200, Constants.FLOOR_TYPE_3);
		floor_2 = new Floor(700, 0, 250, 100, Constants.FLOOR_TYPE_2);
		floor_3 = new Floor(1000, 0, 500, 100, Constants.FLOOR_TYPE_4);
		floor_4 = new Floor(1600, 0, 250, 200, Constants.FLOOR_TYPE_1);
		
		cloud_1 = new Cloud(Constants.CLOUD_TYPE_1, 100, 700);
//		cloud_2 = new Cloud(Constants.CLOUD_TYPE_2, 400, 500);
		cloud_3 = new Cloud(Constants.CLOUD_TYPE_1, 1920 - 350, 850);
		cloud_4 = new Cloud(Constants.CLOUD_TYPE_2, 1000, 300);
	}
	
	private void updateObjects() {
		batch.draw(Assets.getFloorType(floor_1.type), floor_1.position.x, floor_1.position.y, floor_1.bounds.width, floor_1.bounds.height);
		batch.draw(Assets.getFloorType(floor_2.type), floor_2.position.x, floor_2.position.y, floor_2.bounds.width, floor_2.bounds.height);
		batch.draw(Assets.getFloorType(floor_3.type), floor_3.position.x, floor_3.position.y, floor_3.bounds.width, floor_3.bounds.height);
		batch.draw(Assets.getFloorType(floor_4.type), floor_4.position.x, floor_4.position.y, floor_4.bounds.width, floor_4.bounds.height);
		
		batch.draw(Assets.getCloudType(cloud_1.type), cloud_1.position.x, cloud_1.position.y, 350, 200);
//		batch.draw(Assets.getCloudType(cloud_2.type), cloud_2.position.x, cloud_2.position.x, 500, 500);
		batch.draw(Assets.getCloudType(cloud_3.type), cloud_3.position.x, cloud_3.position.y, 350, 200);
		batch.draw(Assets.getCloudType(cloud_4.type), cloud_4.position.x, cloud_4.position.x, 350, 200);
	}

	@Override
	public void render(float delta) {
		update(delta);
		draw(delta);
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public InputProcessor getInputProcessor () {
		return stage;
	}

	@Override
	public void show () {
		stage = new Stage();
		rebuildStage();
	}

	public void rebuildStage() {
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

}
