package com.me.cabalga.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;


/**
 * Clase que controla el estado de la cámara
 * @author Jaime Sierra, Emilio López,  Emilio José Martí, Álvaro Duart
 *
 */
public class CameraHelper {
	
	private final float MAX_ZOOM_IN = 0.25f;
	private final float MAX_ZOOM_OUT = 10.0f;
	private Vector2 position;
	private float zoom;
	private Sprite target;
	
	
	/**
	 * Crea cámara con zoom = 1f por defecto.
	 */
	public CameraHelper () {
		position = new Vector2();
		zoom = 1.0f;
	}
	
	/**
	 * Actualiza el estado de la cámara, posición
	 * @param deltaTime tiempo transcurrido desde la última actualización del juego
	 */
	public void update (float deltaTime) {
		if (!hasTarget()) return;
			position.x = target.getX() + target.getOriginX();
			position.y = target.getY() + target.getOriginY();
	}
	
	/**
	 * Modifica posición de la cámara
	 * @param x posición de coordenadas de la cámara
	 * @param y posición de ordenadas de la cámara
	 */
	public void setPosition (float x, float y) {
		this.position.set(x, y);
	}
	
	/**
	 * Devuelve posición de la cámara
	 * @return Vector2
	 */
	public Vector2 getPosition () { 
		return position; 
	}
	
	/**
	 * Añade zoom a la cámara
	 * @param amount incremento del zoom
	 */
	public void addZoom (float amount) { 
		setZoom(zoom + amount); 
	}
	
	/**
	 * Modifica zoom de la cámara
	 * @param zoom valor de zoom
	 */
	public void setZoom (float zoom) {
		this.zoom = MathUtils.clamp(zoom, MAX_ZOOM_IN, MAX_ZOOM_OUT);
	}
	
	/**
	 * Devuelve valor del zoom
	 * @return zoom
	 */
	public float getZoom () { 
		return zoom; 
	}
	
	/**
	 * Enfocamos a un objetivo
	 * @param target objetivo enfocado, tipo Sprite
	 */
	public void setTarget (Sprite target) { 
		this.target = target;
	}
	
	/**
	 * Nos devuelve el objeto enfocado actualmente
	 * @return target
	 */
	public Sprite getTarget () { 
		return target; 
	}
	
	/**
	 * Comprobamos si estamos enfocando a algo
	 * @return target
	 */
	public boolean hasTarget () { 
		return target != null; 
	}
	
	/**
	 *  Comprobamos si estamos enfocando a un objeto en concreto
	 * @param target objeto del que queremos comprabar su enfoque
	 * @return target
	 */
	public boolean hasTarget (Sprite target) {
		return hasTarget() && this.target.equals(target);
	}
	
	/**
	 * Clonación de nuestra cámara a otra mediante posición y zoom
	 * @param camera cámara que va a recibir nuestro estado
	 */
	public void applyTo (OrthographicCamera camera){
		camera.position.x = position.x;
		camera.position.y = position.y;
		camera.zoom = zoom;
		camera.update();
	}
}