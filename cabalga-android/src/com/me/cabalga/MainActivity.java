package com.me.cabalga;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.swarmconnect.Swarm;


public class MainActivity extends AndroidApplication {
	

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = true;
        
        

        initialize(new Cabalga(), cfg);

        Swarm.setActive(this);
    }
    

    
    public void onResume(){
    	super.onResume();


    	
   		Swarm.setActive(this);
    	Swarm.setAllowGuests(true);
    	Swarm.init(this, 10068, "be4b75871f047da600b41bd07c152f87" );


    }
    
    public void onPause(){
    	super.onPause();
    	Swarm.setInactive(this);
    }
}